/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka7;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Jati
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private TextField tekstikentta;
    @FXML
    private Button painikeT3;
    @FXML
    private Label teksti2;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Hello World!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void onActionT3(ActionEvent event) {
        label.setText(tekstikentta.getText());
    }

    private void labelChange(InputMethodEvent event) {
        teksti2.setText(tekstikentta.getText());
    }

    @FXML
    private void labelChange(KeyEvent event) {
        teksti2.setText(tekstikentta.getText());
    }

    @FXML
    private void labelChange(ActionEvent event) {
        teksti2.setText(tekstikentta.getText());
    }
    
}
